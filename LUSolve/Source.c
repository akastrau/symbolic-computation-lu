#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

const double epsilon = 1e-12;

/*
	OBLICZENIA SYMBOLICZNE
	Rozwiazywanie ukladow rownan liniowych za pomoca metody rozkladu LU

	Przygotowal: Adrian Kastrau

*/
//Macierz A - wyrazy wolne, B - rozwiazania

void Print_Matrix(double **Matrix, size_t MatrixDim);

void ShowLU(double **Matrix, double **L, double **U, size_t MatrixDim) //Pokazanie rozkladu LU (wersja standardowa) - Algorytm Doolittle'a
{

	double sum = 0;

	for (size_t k = 0; k < MatrixDim; k++)
	{
		L[k][k] = 1;

		for (size_t j = k; j < MatrixDim; j++)
		{
			sum = 0;

			for (size_t s = 0; s < k; s++) 
			{
				sum += L[k][s] * U[s][j];
			}

			U[k][j] = Matrix[k][j] - sum;
		}

		for (size_t i = k + 1; i < MatrixDim; i++)
		{
			long double sum = 0;

			for (size_t s = 0; s < k; s++)
			{
				sum += L[i][s] * U[s][k];
			}

			L[i][k] = (Matrix[i][k] - sum) / U[k][k];

		}
	}

	
	printf("Macierz L:\n");

	Print_Matrix(L, MatrixDim);

	printf("Macierz U:\n");

	Print_Matrix(U, MatrixDim);
}

bool Doolitle_Alg(double **Matrix, size_t MatrixDim) //Rozklad LU w miejscu (Macierz wejsciowa jest "niszczona" - nie pokazuje rozkladu
{
	for (size_t i = 0; i < MatrixDim - 1; i++)
	{
		if (fabs(Matrix[i][i]) < epsilon)
		{
			return false;
		}

		else
		{
			for (size_t j = i + 1; j < MatrixDim; j++)
			{
				Matrix[j][i] = Matrix[j][i] / Matrix[i][i];
			}
			
			for (size_t j = i + 1; j < MatrixDim; j++)
			{
				for (size_t k = i + 1; k < MatrixDim; k++)
				{
					Matrix[j][k] = Matrix[j][k] - (Matrix[j][i] * Matrix[i][k]);

				}
				
			}
			
		}
		
	}

	return true;
}

bool LUSolve(double **Matrix, double *ColB, double *ColX, size_t MatrixDim) //Pokazanie rozwiazan (trzeba wywolac Doolittle_Alg)
{
	double sum = 0;

	ColX[0] = ColB[0];

	for (size_t i = 1; i < MatrixDim; i++)
	{
		sum = 0;

		for (size_t j = 0; j < i; j++)
		{
			sum = sum + (Matrix[i][j] * ColX[j]);
		}

		ColX[i] = ColB[i] - sum;
	}

	if (fabs(Matrix[MatrixDim - 1][MatrixDim - 1]) < epsilon)
	{
		return false;
	}

	ColX[MatrixDim - 1] = ColX[MatrixDim - 1] / Matrix[MatrixDim - 1][MatrixDim - 1];

	for (int i = MatrixDim - 2; i >= 0; i--)
	{
		sum = 0;

		for (size_t j = i + 1; j < MatrixDim; j++)
		{
			sum = sum + (Matrix[i][j] * ColX[j]);
		}

		if (fabs(Matrix[i][i]) < epsilon)
		{
			return false;
		}

		ColX[i] = (ColX[i] - sum) / Matrix[i][i];
	}

	return true;

}

void Print_Matrix(double **Matrix, size_t MatrixDim)
{
	printf("\n\n");

	for (size_t i = 0; i < MatrixDim; i++)
	{
		for (size_t j = 0; j < MatrixDim; j++)
		{
			printf("%lf ", Matrix[i][j]);
		}
		printf("\n");
	}
	printf("\n\n");
}

void Print_Message(void)
{
	printf("\n\n\t\tPROJEKT - OBLICZENIA SYMBOLICZNE\n\t\tROZWIAZYWANIE UKLADOW ROWNAN LINIOWYCH ZA POMOCA ROZKLADU LU\n\t\tADRIAN KASTRAU\n\t===================================================================\n\n");
}

void Matrix_multiplication(double **A, double **B, double **C, size_t MatrixDim) //Mnozenie macierzy (standardowe)
{
	for (size_t j = 0; j < MatrixDim; j++)
	{
		for (size_t i = 0; i < MatrixDim; i++)
		{
			for (size_t k = 0; k < MatrixDim; k++)
			{
				C[i][j] += A[i][k] * B[k][j];
			}
				
		}
			
	}
		
}

int main(void)
{
	system("TITLE ROZWIAZYWANIE UKLADOW ROWNAN LINIOWYCH ZA POMOCA ROZKLADU LU");

	size_t MatrixDim = 0;
	bool CheckInput = false;

	Print_Message();

	while (!CheckInput)
	{
		printf("\n\nWpisz wymiar macierzy: ");
		CheckInput = scanf_s("%i", &MatrixDim);
		_flushall();
	}
	
	double **Matrix = NULL; 
	double **L = NULL;
	double **U = NULL;
	double **Matrix_before_decomposition = NULL;
	double *A = NULL, *B = NULL;

	A = (double*)calloc(MatrixDim, sizeof(double));

	B = (double*)calloc(MatrixDim, sizeof(double));

	if (A == NULL || B == NULL)
	{
		printf("Brak pamieci!\n\n");
		system("pause");
		return EXIT_FAILURE;
	}

	Matrix = (double**)calloc(MatrixDim, sizeof(double*));
	L = (double**)calloc(MatrixDim, sizeof(double*));
	U = (double**)calloc(MatrixDim, sizeof(double*));

	for (size_t i = 0; i < MatrixDim; i++)
	{
		Matrix[i] = (double*)calloc(MatrixDim, sizeof(double));
		L[i] = (double*)calloc(MatrixDim, sizeof(double));
		U[i] = (double*)calloc(MatrixDim, sizeof(double));

		if (Matrix[i] == NULL || L[i] == NULL || U[i] == NULL)
		{
			printf("Out of memory!\n\n");
			system("pause");
			return EXIT_FAILURE;
		}
	}

	Matrix_before_decomposition = (double**)calloc(MatrixDim, sizeof(double*));

	for (size_t i = 0; i < MatrixDim; i++)
	{
		Matrix_before_decomposition[i] = (double*)calloc(MatrixDim, sizeof(double));

		if (Matrix_before_decomposition[i] == NULL)
		{
			printf("Brak pamieci!\n\n");
			system("pause");
			return EXIT_FAILURE;
		}
	}

	for (size_t i = 0; i < MatrixDim; i++)
	{
		printf("\n\nWpisz wiersz %i \n(liczba zatwierdzana klawiszem ENTER; wraz z kolumna wyrazow wolnych):\n", i + 1);

		size_t count = 0;
		CheckInput = false;

		while (count != MatrixDim + 1)
		{
			if (count != MatrixDim)
			{
				CheckInput = scanf_s("%lf", &Matrix[i][count]);

				if (CheckInput)
				{
					Matrix_before_decomposition[i][count] = Matrix[i][count];
				}
			}

			else
			{
				CheckInput = scanf_s("%lf", &A[i]);
			}
			
			if (!CheckInput)
			{
				printf("\nBlad wprowadzania!\n\nWprowadz jeszcze %i liczbe/by\n\nSprobuj ponownie!\n\n", MatrixDim + 1 - count );
				_flushall();
			}
			else
			{
				count++;
			}
		}
	}

	printf("\n\nWprowadzona macierz:\n\n");

	for (size_t i = 0; i < MatrixDim; i++)
	{
		for (size_t j = 0; j < MatrixDim; j++)
		{
			printf("%lf ", Matrix[i][j]);
		}
		printf("\n");
	}

	printf("\n\n");

	if (Doolitle_Alg(Matrix, MatrixDim) && LUSolve(Matrix, A, B, MatrixDim)) //Jezeli da sie rozlozyc macierz i wyznaczyc rozwiazania 
	{
		ShowLU(Matrix_before_decomposition, L, U, MatrixDim); //Pokaz rozklad L U

		for (size_t i = 0; i < MatrixDim; i++)
		{
			for (size_t j = 0; j < MatrixDim; j++)
			{
				Matrix_before_decomposition[i][j] = 0; //Zerowanie macierzy (potrzebne do sprawdzenia czy rozklad jest poprawny)
			}
		}

		Matrix_multiplication(L, U, Matrix_before_decomposition, MatrixDim); //Mnozenie macierzy L x U. W wyniku powinno sie otrzymac macierz wejsciowa (bez kolumny wyrazow wolnych)

		printf("Wynik mnozenia macierzy L x U:\n");

		Print_Matrix(Matrix_before_decomposition, MatrixDim);
		
		printf("\n\nRozwiazanie ukladu rownan:\n\n"); //Pokaz rozwiazania 
		for (size_t i = 0; i < MatrixDim; i++)
		{
			printf("x%i = %lf\n", i + 1, B[i]);
		}
		printf("\n\n");
	}

	else
	{
		printf("Obliczenie rozwiazan niemozliwe! Byc moze inny algorytm dalby rade! :)\n\n");
		free(Matrix);
		free(Matrix_before_decomposition);
		free(A);
		free(B);
		free(L);
		free(U);

		system("pause");
		return EXIT_FAILURE;
	}


	free(Matrix);
	//free(Matrix_before_decomposition);
	free(A);
	free(B);
	free(L);
	free(U);
	
	system("pause");
	return EXIT_SUCCESS;
}
